local mymodule = {}

-- Load libraries
modelfunctions = require("modelfunctions")
posix = require("posix")
processinfo = require("acf.processinfo")
fs = require("acf.fs")
validator = require("acf.validator")

-- Set variables
local processname = "pppd"
local packagename = "ppp"
local papfile = "/etc/ppp/pap-secrets"
local chapfile = "/etc/ppp/chap-secrets"
local peerspath = "/etc/ppp/peers/"

local path = "PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin "

-- ################################################################################
-- LOCAL FUNCTIONS

-- ################################################################################
-- PUBLIC FUNCTIONS

function mymodule.getstatus()
	local status = {}

	local value, errtxt = processinfo.package_version(packagename)
	status.version = cfe({
		label="Program version",
		value=value,
		errtxt=errtxt,
		name=packagename
	})

	status.status = cfe({ label = "Program status", name=processname })
	local t = processinfo.pidof(processname)
	if (t) and (#t > 0) then
		status.status.value = "Started"
	else
		status.status.value = "Stopped"
	end

	return cfe({ type="group", value=status, label="PPP Status" })
end

function mymodule.read_papfiledetails()
	return modelfunctions.getfiledetails(papfile)
end

function mymodule.update_papfiledetails(self, filedetails)
	local retval = modelfunctions.setfiledetails(self, filedetails, {papfile})
	posix.chmod(papfile, "rw-------")
	return retval
end

function mymodule.read_chapfiledetails()
	return modelfunctions.getfiledetails(chapfile)
end

function mymodule.update_chapfiledetails(self, filedetails)
	local retval = modelfunctions.setfiledetails(self, filedetails, {chapfile})
	posix.chmod(chapfile, "rw-------")
	return retval
end

function mymodule.list_peers()
	return cfe({ type="list", value=fs.find_files_as_array(nil, peerspath), label="Peers" })
end

function mymodule.get_newpeer()
	local newpeer = {}
	newpeer.name = cfe({ label="Name" })
	return cfe({ type="group", value=newpeer, label="New Peer" })
end

function mymodule.create_peer(self, newpeer)
	newpeer.errtxt = "Failed to create peer"
	local path = newpeer.value.name.value
	if not string.find(path, "/") then
		path = peerspath .. path
	end

	if validator.is_valid_filename(path, peerspath) then
		if (posix.stat(path)) then
			newpeer.value.name.errtxt = "Peer already exists"
		else
			fs.create_file(path)
			newpeer.errtxt = nil
		end
	else
		newpeer.value.name.errtxt = "Invalid name"
	end

	return newpeer
end

function mymodule.get_delete_peer(self, clientdata)
	retval = {}
	retval.name = cfe({ value=clientdata.name or "", label="Name" })
	return cfe({ type="group", value=retval, label="Delete Peer File" })
end

function mymodule.delete_peer(self, delpeer)
	delpeer.errtxt = "Failed to delete peer"
	delpeer.value.name.errtxt="Peer not found"

	for file in fs.find(nil, peerspath) do
		if delpeer.value.name.value == file then
			os.remove(file)
			delpeer.errtxt = nil
			delpeer.value.name.errtxt = nil
			break
		end
	end

	return delpeer
end

function mymodule.read_peerfile(name)
	return modelfunctions.getfiledetails(name, fs.find_files_as_array(nil, peerspath))
end

function mymodule.update_peerfile(self, filedetails)
	return modelfunctions.setfiledetails(self, filedetails, fs.find_files_as_array(nil, peerspath))
end

return mymodule
