local mymodule = {}

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.editpap(self)
	return self.handle_form(self, self.model.read_papfiledetails, self.model.update_papfiledetails, self.clientdata, "Save", "Edit Config", "Configuration Set")
end

function mymodule.editchap(self)
	return self.handle_form(self, self.model.read_chapfiledetails, self.model.update_chapfiledetails, self.clientdata, "Save", "Edit Config", "Configuration Set")
end

function mymodule.listpeers(self)
	return self.model.list_peers()
end

function mymodule.createpeer(self)
	return self.handle_form(self, self.model.get_newpeer, self.model.create_peer, self.clientdata, "Create", "Create New Peer File", "Peer File Created")
end

function mymodule.deletepeer(self)
	return self.handle_form(self, self.model.get_delete_peer, self.model.delete_peer, self.clientdata, "Delete", "Delete Peer File", "Peer File Deleted")
end

function mymodule.editpeer(self)
	return self.handle_form(self, function() return self.model.read_peerfile(self.clientdata.name) end, self.model.update_peerfile, self.clientdata, "Save", "Edit PPP Peer", "Peer Configuration Set")
end

return mymodule
