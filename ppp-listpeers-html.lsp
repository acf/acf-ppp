<% local form, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#list").tablesorter({widgets: ['zebra']});
	});
</script>

<% htmlviewfunctions.displaycommandresults({"deletepeer", "editpeer"}, session) %>
<% htmlviewfunctions.displaycommandresults({"createpeer"}, session, true) %>

<% local header_level = htmlviewfunctions.displaysectionstart(form, page_info) %>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>Peer File</th>
	</tr>
</thead><tbody>
<% local name = cfe({ type="hidden", value="" }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for i,file in ipairs(form.value) do %>
	<tr>
		<td>
			<%
			name.value = file
			if viewlibrary.check_permission("deletepeer") then
				htmlviewfunctions.displayitem(cfe({type="form", value={name=name}, label="", option="Delete", action="deletepeer"}), page_info, -1)
			end
			if viewlibrary.check_permission("editpeer") then
				htmlviewfunctions.displayitem(cfe({type="link", value={name=name, redir=redir}, label="", option="Edit", action="editpeer"}), page_info, -1)
			end
			%>
		</td>
		<td><%= html.html_escape(string.gsub(file, "^.*/", "")) %></td>
	</tr>
<% end %>
</tbody></table>
<% htmlviewfunctions.displaysectionend(header_level) %>

<% if viewlibrary and viewlibrary.dispatch_component and viewlibrary.check_permission("createpeer") then
	viewlibrary.dispatch_component("createpeer")
end %>
